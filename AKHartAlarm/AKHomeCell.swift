//
//  AKHomeCell.swift
//  AKHartAlarm
//
//  Created by 李亚坤 on 2017/3/31.
//  Copyright © 2017年 Kuture. All rights reserved.
//

import UIKit

protocol HomecellDelegate:NSObjectProtocol {
    
    func addPlus( _ indexP:Int)
}


class AKHomeCell: UITableViewCell {

    weak var delegateHome:HomecellDelegate?
    
    lazy var timesLabel: UILabel = {
        
       let timesLabel = UILabel()
        timesLabel.frame = CGRect(x: 10, y: 0, width: 200, height: 50)
        timesLabel.textAlignment = NSTextAlignment.left
        timesLabel.font = UIFont.systemFont(ofSize: 15)
       
        return timesLabel
    }()
    
    lazy var alarmCount: UILabel = {
        
        var alarmCount = UILabel()
        alarmCount.frame = CGRect(x: SCREAN_WIDTH - 90, y: 18, width: 50, height: 15)
        alarmCount.textColor = UIColor.red
        alarmCount.font = UIFont.systemFont(ofSize: 15)
        alarmCount.textAlignment = NSTextAlignment.center
        
        return alarmCount
    }()
    
    var indexPathHome:Int? = 0
    
    lazy var add: UIButton = {
        
       let add = UIButton()
        
        add.setImage(UIImage.init(named: "284"), for: UIControlState.normal)
        add.frame = CGRect(x: SCREAN_WIDTH - 40 , y: 10, width: 30, height: 30)
        add.addTarget(self, action: #selector(plusBtnMethod), for: UIControlEvents.touchUpInside)
        
        return add
    }()
    
    //视图初始化
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        contentView.addSubview(timesLabel)
        contentView.addSubview(alarmCount)
        contentView.addSubview(add)
    }
    
    //代理方法
    func plusBtnMethod(){
        
        delegateHome?.addPlus(indexPathHome!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
