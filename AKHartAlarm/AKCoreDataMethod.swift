//
//  AKCoreDataMethod.swift
//  AKHartAlarm
//
//  Created by 李亚坤 on 2017/3/30.
//  Copyright © 2017年 Kuture. All rights reserved.
//

import UIKit
import CoreData

class AKCoreDataMethod: NSObject {
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    let EntityName = "HartRecode"
    let times = "times"
    let alarmCount = "alarmCount"
    
    // 增加数据
    func storePerson(name:String, age:Int){
        let context = getContext()
        // 定义一个entity，这个entity一定要在xcdatamodeld中做好定义
        let entity = NSEntityDescription.entity(forEntityName: EntityName, in: context)
        
        let person = NSManagedObject(entity: entity!, insertInto: context)
  
        person.setValue(name, forKey: times)
        person.setValue(age, forKey: alarmCount)
        
        do {
            try context.save()
            print("saved")
        }catch{
            print(error)
        }
    }
    
    // 查找数据
    func getPerson() ->NSArray{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName)
        var timesArray = [String]()
        var alarmArray = [Float]()
        var mainArray = [NSArray]()
        
        do {
            
            let searchResults = try getContext().fetch(fetchRequest)
            
            for p in (searchResults as! [NSManagedObject]){

                timesArray.append(p.value(forKey: times)! as! String)
                alarmArray.append(p.value(forKey: alarmCount) as! Float)
            }
            
        } catch  {
            
            print(error)
        }
        
        mainArray.append(timesArray as NSArray)
        mainArray.append(alarmArray as NSArray)
        
        return mainArray as NSArray!
    }
    
    //删除数据
    func coredateDelet( _ index:Int){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName)

        do {
            
            var searchResults = try getContext().fetch(fetchRequest)
            if searchResults.count != 0{
                
                getContext().delete(searchResults[index] as! NSManagedObject)
                try getContext().save()
            }else{
                
                print("数值不在")
            }
            
        } catch  {
            
            print(error)
        }
    }
    
    //修改数据
    func coredateCorrect( _ corrTimes:String, _ corrAlarm:Float, _ corrCount:Int){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName)
        
        do {
            
            let searchResults = try getContext().fetch(fetchRequest)
           
            (searchResults[corrCount] as AnyObject).setValue(corrAlarm, forKey: alarmCount)
            try getContext().save()
            
        } catch {
            
            print(error)
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
