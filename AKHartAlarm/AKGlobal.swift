//
//  AKGlobal.swift
//  AKHartAlarm
//
//  Created by 李亚坤 on 2017/3/31.
//  Copyright © 2017年 Kuture. All rights reserved.
//

import UIKit

let SCREAN_WIDTH = UIScreen.main.bounds.size.width
let SCREAN_HEIGHT = UIScreen.main.bounds.size.height

func fontSiz ( _ size:CGFloat) -> UIFont{
    
    return UIFont.systemFont(ofSize: size)
}

