//
//  AKHomeController.swift
//  AKHartAlarm
//
//  Created by 李亚坤 on 2017/3/30.
//  Copyright © 2017年 Kuture. All rights reserved.
//

import UIKit

class AKHomeController: UITableViewController,HomecellDelegate {

    fileprivate lazy var leftItem:UIBarButtonItem = {
        
        let leftItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(addHartRecodeCell))
        
        return leftItem
    }()
    
    fileprivate lazy var rightItem:UIBarButtonItem = {
        
        let rightItem = UIBarButtonItem(title: "数据分析", style: UIBarButtonItemStyle.done, target: self, action: #selector(editCurrentCell))
        
        return rightItem
    }()
    
    var dataMainArray:NSArray? = nil
    var timesArray = [String]()
    var alarmArray = [Float]()
    var editStatus = false
    let coreData = AKCoreDataMethod()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        homeInitSetting()
        setConfigUI()
    }
    
    //初始化
    func homeInitSetting(){
        
        title = "心颤记录"
        tableView.rowHeight = 50
        
        dataMainArray = coreData.getPerson()
        timesArray = (dataMainArray?[0])! as? NSArray as! [String]
        alarmArray = (dataMainArray?[1])! as? NSArray as! [Float]
    }
    
    //视图初始化
    func setConfigUI(){
        
        self.navigationItem.leftBarButtonItem = leftItem
        self.navigationItem.rightBarButtonItem = rightItem
    }

    //添加记录值
    func addHartRecodeCell() {
        
        let date = NSDate()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyy-MM-dd HH:mm"
        let strNowTime = timeFormatter.string(from: date as Date) as String
        
        coreData.storePerson(name: strNowTime, age: 0)
        
        dataMainArray = coreData.getPerson()
        timesArray = (dataMainArray?[0])! as? NSArray as! [String]
        alarmArray = (dataMainArray?[1])! as? NSArray as! [Float]
        
        tableView.reloadData()
    }
    
    //编辑当前记录
    func editCurrentCell( _ items:UIBarButtonItem) {
        
       let analyse = AKAnalyse()
        analyse.title = "数据分析"
        self.navigationController?.pushViewController(analyse, animated: true)
    }

    //添加数据源方法
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (timesArray.count)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AKHomeCell()
        cell.delegateHome = self

        cell.indexPathHome = indexPath.row
        cell.timesLabel.text = timesArray[indexPath.row]
        cell.alarmCount.text = String(describing: Int(alarmArray[indexPath.row]))
        

        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        
        return UITableViewCellEditingStyle.delete
    }
    
    //删除指定数据
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.delete {
            
            timesArray.remove(at: indexPath.row)
            alarmArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            coreData.coredateDelet(indexPath.row)
//            homeInitSetting()
        }
        
        tableView.reloadData()
    }
    
    //实现代理plus按钮点击方法
    func addPlus(_ indexP: Int) {

       homeInitSetting()
        var currentNum:Float? = 0
        currentNum = alarmArray[indexP]
        currentNum = currentNum! + 1
      
        coreData.coredateCorrect("", currentNum!, indexP)
        homeInitSetting()
        tableView.reloadData()
    }
    
    
    
    
    
    
    
    
    
    

}
